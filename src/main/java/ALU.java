import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by vbustamante on 21/12/16.
 */

class ALU implements ClockedComponent{
  private int A, B; //Inputs
  private int X; //Output
  private byte OP; //Operation selection is actually 4 bits, but the smallest we can get in java is a byte so this will do.

  ObservableList<String> operations =
      FXCollections.observableArrayList(
          "0000: 0",
          "0001: A + B",
          "0010: A - B",
          "0011: -B",
          "0100: A and B",
          "0101: A or B",
          "0110: A xor B",
          "0111: not B",
          "1000: not A",
          "1001: B",
          "1010: shiftL A",
          "1011: shiftR A",
          "1100: rotateL A",
          "1101: rotateR A",
          "1110: A + 4",
          "1111: A"
      );

  void setA(int a){
    A = a;
  }
  void setB(int b){
    B = b;
  }
  void setOP(byte op){
    OP = op;
  }

  public void clock(){X = execute();}

  int getX(){return X;}

  private int execute(){
    OP &= 0xF; //Ensure OP is nibble-sized
    int val;
    switch (OP){
      case 0:
        val = 0;
        break;
      case 1:
        val = A+B;
        break;
      case 2:
        val = A-B;
        break;
      case 3:
        val = -B;
        break;
      case 4:
        val = A & B;
        break;
      case 5:
        val = A | B;
        break;
      case 6:
        val = A ^ B;
        break;
      case 7:
        val = ~B;
        break;
      case 8:
        val = ~A;
        break;
      case 9:
        val = B;
        break;
      case 10:
        val = A << 1;
        break;
      case 11:
        val = A >> 1;
        break;
      case 12:
        val = (A << 1) | (A >> 31);
        break;
      case 13:
        val = (A >> 1) | (A << 31);
        break;
      case 14:
        val = A + 4;
        break;
      default:
        val = A;
    }
    return val;
  }
}
