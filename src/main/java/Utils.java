/**
 * Created by vbustamante on 21/12/16.
 */
final class Utils {
  static String intTo8HexString(int i){
    return String.format("%8s", Integer.toHexString(i)).replace(' ', '0').toUpperCase();
  }
}
