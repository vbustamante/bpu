/**
 * Created by vbustamante on 21/12/16.
 * This interface ensures we can batch execute all clock methods after setting inputs on all components,
 * emulating the workings of the hardware clock
 */
public interface ClockedComponent {
  void clock();
}
