import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class BPU  extends Application{

  @Override
  public void start(Stage primaryStage){

    GridPane grid = new GridPane();
    grid.setAlignment(Pos.CENTER);
    grid.setHgap(10);
    grid.setVgap(10);
    grid.setPadding(new Insets(25));

    Button showAlu = new Button("ALU");
    showAlu.setOnAction(e->showAluInterface());
    grid.add(showAlu, 1, 4);

    //Scene scene = new Scene(grid, 300, 275);
    Scene scene = new Scene(grid);
    primaryStage.setScene(scene);
    primaryStage.setTitle("Braffman Processing Unit");
    primaryStage.show();

    //TODO remove direct call when finished with alu interface
    showAluInterface();
  }

  private void showAluInterface() {
    AluInterface aluInterface = new AluInterface();
    aluInterface.start();
  }


  public static void main(String args[]) {
    launch(args);
  }
}
