import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Created by vbustamante on 21/12/16.
 */
class AluInterface {
  private ALU alu = new ALU();

  private TextField x;//TODO class accessible
  void start(){
    Stage stage = new Stage();

    GridPane grid = new GridPane();
    grid.setAlignment(Pos.CENTER);
    grid.setHgap(10);
    grid.setVgap(10);
    grid.setPadding(new Insets(25));

    Label aLabel = new Label("A");
    TextField a = new TextField();
    grid.add(aLabel, 0, 0);
    grid.add(a, 1, 0);
    a.textProperty().addListener(
        (final ObservableValue<? extends String> observableValue,
         final String oldValue, final String newValue) -> {
          String filtered;
          filtered = newValue;
          if(filtered.length()>8){
            filtered = filtered.substring(0, 8);
            System.out.println("Too big");
          }
          filtered = filtered.toUpperCase();
          filtered = filtered.replaceAll("[^A-F|0-9]", "");
          if(filtered.length() > 0) {
            Long hexContainer = Long.parseLong(filtered, 16);
            //We have to parse hex literal as long since Java treats any literal as positive, and any value above
            //0x80000000 is to big to fit in the int type. Getting the intValue from the long converts correctly.
            alu.setA(hexContainer.intValue());
          }else{
            alu.setA(0);
          }
          a.setText(filtered);
          refresh();
      }
    );

    Label bLabel = new Label("B");
    TextField b = new TextField();
    grid.add(bLabel, 0, 1);
    grid.add(b, 1, 1);
    b.textProperty().addListener(
        (final ObservableValue<? extends String> observableValue,
         final String oldValue, final String newValue) -> {
          String filtered;
          filtered = newValue;
          if(filtered.length()>8){
            filtered = filtered.substring(0, 8);
            System.out.println("Too big");
          }
          filtered = filtered.toUpperCase();
          filtered = filtered.replaceAll("[^A-F|0-9]", "");
          if(filtered.length() > 0) {
            Long hexContainer = Long.parseLong(filtered, 16);
            //We have to parse hex literal as long since Java treats any literal as positive, and any value above
            //0x80000000 is to big to fit in the int type. Getting the intValue from the long converts correctly.
            alu.setB(hexContainer.intValue());
          }else{
            alu.setB(0);
          }
          b.setText(filtered);
          refresh();
        }
    );

    Label opLabel = new Label("OP");
    grid.add(opLabel, 0, 2);
    //final ComboBox op = new ComboBox<>(aluOps);
    ComboBox op = new ComboBox<>(alu.operations);
    op.getSelectionModel().selectFirst();
    grid.add(op, 1, 2);//TODO: Set monospace font(e.g. courier) for op combobox so list doesn't get misaligned
    op.setOnAction(e->{
      System.out.println("Selected" + op.getSelectionModel().getSelectedIndex());
      alu.setOP((byte) op.getSelectionModel().getSelectedIndex());
      refresh();
    });




    Label xLabel = new Label("X");
    x = new TextField();
    grid.add(xLabel, 0, 3);
    grid.add(x, 1, 3);
    x.setDisable(true);
    x.setText(Utils.intTo8HexString(alu.getX()));

    //Scene scene = new Scene(grid, 300, 275);
    Scene scene = new Scene(grid);
    stage.setScene(scene);
    stage.setTitle("Alu");
    stage.show();
  }

  private void refresh(){
    alu.clock();
    x.setText(Utils.intTo8HexString(alu.getX()));
  }
}